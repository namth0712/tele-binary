import moment from 'moment-timezone';
import telegramLoader from './telegram';
import senity from '../apps/senity';

moment.tz.setDefault('Asia/Ho_Chi_Minh');
export default async () => {
  const bot = telegramLoader();
  console.info('✌️ Telegram loaded');

  senity(bot);

  return bot;
};
