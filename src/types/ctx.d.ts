import { ContextMessageUpdate } from 'telegraf';

export interface MContext extends ContextMessageUpdate {
  state?: {
    command: {
      raw?: string;
      command?: string;
      args?: Array<string>;
    };
  };
}

export type Next = () => any;
