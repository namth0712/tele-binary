declare module 'msgpack-javascript' {
  export class Unpacker {
    constructor(someParam?: Uint8Array);

    unpackNil: () => number;
    unpackBoolean: () => boolean;
    unpackInt: () => number;
    unpackFloat: () => number;
    unpackDouble: () => number;
    unpackString: () => string;
    unpackArray: () => Array<any>;
    unpackBinary: () => number;
    unpackMap: () => any;
  }
  export default noTypesYet;
}
