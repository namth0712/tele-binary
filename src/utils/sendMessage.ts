import Telegraf, { ContextMessageUpdate } from 'telegraf';

type ParseMode = 'HTML';

export default (
  bot: Telegraf<ContextMessageUpdate>,
  chatId: number | string,
  message: string,
  isCodeBlock = true,
  extraParams = {},
) => {
  /* eslint-disable @typescript-eslint/camelcase */
  const type: ParseMode = 'HTML';
  const extra = {
    parse_mode: type,
    disable_web_page_preview: true,
    ...extraParams,
  };
  if (isCodeBlock) {
    message = `<pre>${message}</pre>`;
  }
  bot.telegram.sendMessage(chatId, message, extra);
};
