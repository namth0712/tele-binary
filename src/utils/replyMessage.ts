import { ContextMessageUpdate } from 'telegraf';

type ParseMode = 'HTML';

export default (
  ctx: ContextMessageUpdate,
  message: string,
  isCodeBlock = false,
  extraParams = {},
) => {
  /* eslint-disable @typescript-eslint/camelcase */
  const type: ParseMode = 'HTML';
  const extra = {
    parse_mode: type,
    disable_web_page_preview: true,
    ...extraParams,
  };
  if (isCodeBlock) {
    message = `<pre>${message}</pre>`;
  }

  ctx.reply(message, extra);
};
