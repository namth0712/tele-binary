import getVal from 'get-value';
import setVal from 'set-value';

const storageObj = {
  inited: false,
};

export default storageObj;

export const getStorage = (key: string, defaultVal: any = '') =>
  getVal(storageObj, key, defaultVal);
export const setStorage = (key: string, val: any) =>
  setVal(storageObj, key, val);
