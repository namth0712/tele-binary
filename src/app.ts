import dotenv from 'dotenv';

const envFound = dotenv.config();
if (!envFound) {
  // This error should crash whole process

  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}
async function startBot() {
  const bot = await require('./loaders').default();

  bot.launch();
}
startBot();
