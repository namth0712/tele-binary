import { BehaviorSubject, interval, Subscription } from 'rxjs';
import { filter, auditTime } from 'rxjs/operators';
import socketIOClient from 'socket.io-client';

let watchInterval: Subscription = null;
const timeoutOffset = 5000;

const startFetch = (processData: (data: any) => void) => {
  const socketUrl = 'https://eth.pricecenter.live';
  // observable of error, disconnect ...
  const ReconnectSubject = new BehaviorSubject(true);

  let socket: any = null;
  let lastUpdateTime = 0;

  const connect = () => {
    socket = socketIOClient(socketUrl, {
      autoConnect: !1,
    });

    socket.on('connect', () => {
      socket.emit('message', 'ok');
      // logMsg('watch_socket', 'start', exchangeId);
    });

    socket.on('chart-data', (data: any) => {
      lastUpdateTime = Date.now();
      processData(data);
    });
    socket.connect();
    return socket;
  };

  // heartbeat
  // every second, check if data is updated
  // if data is not update, fire reconnect request

  // first check if watchInterval is initialed or not
  if (watchInterval) {
    watchInterval.unsubscribe();
  }

  watchInterval = interval(timeoutOffset)
    .pipe(
      filter(
        () => lastUpdateTime && lastUpdateTime < Date.now() - timeoutOffset,
      ),
    )
    .subscribe(() => {
      console.log('Timebit check boutime wrong');
      if (socket) {
        // close last socket
        socket.disconnect();
      }
      ReconnectSubject.next(true);
    });

  //= =========Handle observable fire================

  // handle when reconnect request fired, close connect and re-connect
  const reconnectSub = ReconnectSubject.pipe(
    auditTime(1000), // only receive 1 reconnect request per 1s
  ).subscribe(() => {
    console.log('something wrong, reconnect');
    if (socket) {
      // close last socket
      socket.disconnect();
    }
    // initial new socket
    connect();
  });

  return {
    stopSocket: () => {
      if (reconnectSub.unsubscribe) {
        reconnectSub.unsubscribe();
      }
      if (watchInterval.unsubscribe) {
        watchInterval.unsubscribe();
      }
      if (socket) {
        socket.disconnect();
      }
    },
  };
};
export default startFetch;
