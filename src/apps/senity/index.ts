import Telegraf from 'telegraf';

import { MContext } from '../../types/ctx';
import { replyMessage } from '../../utils';
import { getStorage, setStorage } from '../../storage';

import watchSenity from './watchers/watchSenity';

type ChartDataType = [string, number, number, number, number, number, boolean];

export default (bot: Telegraf<MContext>) => {
  let initialedWatcher: null | { stopSocket: () => void } = null;

  const stopProcess = () => {
    if (initialedWatcher) {
      initialedWatcher.stopSocket();
      initialedWatcher = null;
    }
  };

  const processSocketData = (data: {
    data: Array<[string, number, number, number, number]>;
    serverTime: {
      currentTime: string;
      second: number;
      canOrder: boolean;
    };
  }) => {
    /* const data = {
      data: [
        ['04:30', 203.78, 204.13, 203.89, 204.21],
        ['05:00', 203.58, 203.89, 203.58, 203.93],
      ],
      serverTime: { currentTime: '10:05', second: 17, canSet: true },
    }; */
    /* const today = new Date();
    const date = today.getUTCDate();
    const month = today.getUTCMonth();
    const year = today.getUTCFullYear();
    const hour = today.getUTCHours();
    const minute = today.getUTCMinutes();
    const second = today.getUTCSeconds();
    console.log('===9999===', year, month, date, hour, minute, second);
    const [serverHour, serverMinute] = data.serverTime.currentTime.split(':');
    console.log('===serverHour===', serverHour, serverMinute);

    const timestamp = new Date(
      `${month}/${date}/${year} ${serverHour}:${serverMinute}:${second}`,
    ).getTime();

    console.log('===timestamp===', timestamp); */

    const chartData = data.data;
    let canOrder = data.serverTime.canOrder;

    chartData.pop();
    const lastChartData: Array<ChartDataType> = getStorage('lastChartData', []);

    let tail = false;
    if (lastChartData.length) {
      const lastData = lastChartData[lastChartData.length - 1];
      const checkLastItem = chartData[chartData.length - 1];
      if (
        lastData[0] === checkLastItem[0] &&
        lastData[1] === checkLastItem[1] &&
        lastData[2] === checkLastItem[2] &&
        lastData[3] === checkLastItem[3] &&
        lastData[4] === checkLastItem[4]
      ) {
        tail = true;
      } else {
        for (let i = 0; i < chartData.length; i++) {
          const element = chartData[i];

          const [time, low, open, close, high] = element;
          const isResult = canOrder;
          canOrder = !canOrder;

          if (!tail && lastData[0]) {
            if (
              lastData[0] === time &&
              lastData[1] === low &&
              lastData[2] === open &&
              lastData[3] === close &&
              lastData[4] === high
            ) {
              tail = true;
            }
            continue;
          }
          const data: ChartDataType = [
            time,
            low,
            open,
            close,
            high,
            open < close ? 1 : -1,
            isResult,
          ];
          lastChartData.push(data);
        }
      }
    }

    if (!tail) {
      for (let i = 0; i < chartData.length; i++) {
        const element = chartData[i];
        const [time, low, open, close, high] = element;
        const isResult = canOrder;
        canOrder = !canOrder;
        const data: ChartDataType = [
          time,
          low,
          open,
          close,
          high,
          open < close ? 1 : -1,
          isResult,
        ];
        lastChartData.push(data);
      }
    }
    if (lastChartData.length > 5000) {
      setStorage('lastChartData', lastChartData.slice(-5000));
    } else {
      setStorage('lastChartData', lastChartData);
    }
  };

  const startProcess = () => {
    stopProcess();
    initialedWatcher = watchSenity(processSocketData);
  };

  startProcess();

  const signalToString = (signal: string): string => {
    const match = signal.match(/.{1,2}/g);
    let signalStr = '';
    for (let i = 0; i < match.length; i++) {
      const pair = match[i];
      const type = pair[1];
      if (
        !pair[0] ||
        !type ||
        !parseInt(pair[0]) ||
        (type.toLowerCase() !== 'b' && type.toLowerCase() !== 's')
      ) {
        return signalStr;
      }
      const val = pair[1] == 'b' ? '1' : '0';
      signalStr += val.repeat(parseInt(pair[0]));
    }
    return signalStr;
  };

  bot.command('help', async (ctx: MContext) => {
    let message = '';
    message += 'Welcom to Tele Binary Watch';
    message += `
    -------------------------
    /help - Display help

    👉/setsignal {signal} - Add a signal to watch
    Ex: /setsignal 1b2s2b
    👉/removesignal {signal} - Remove a signal
    Ex: /removesignal 1b2s2b
    👉/viewsignal - View watched signals

    <b>Data Information</b>
    👉/csv - Get current data to excel file
    `;

    replyMessage(ctx, message);
  });

  bot.command('removesignal', ctx => {
    const userId = ctx.update.message.from.id;
    const signals = getStorage(`signalData.${userId}`, {});
    if (!signals) {
      replyMessage(ctx, 'You have no signals');
      return;
    }
    const signalKey = ctx.state.command.args[0];
    if (!signalKey) {
      replyMessage(ctx, 'Please enter a valid signal!');
      return;
    }
    if (!signals[signalKey]) {
      replyMessage(ctx, `You haven't set signal ${signalKey}`);
      return;
    }
    delete signals[signalKey];
    setStorage(`signalData.${userId}`, signals);
    replyMessage(ctx, 'Signal removed successfully!');
  });

  bot.command('setsignal', ctx => {
    const userId = ctx.update.message.from.id;
    let signals = getStorage(`signalData.${userId}`, {});
    if (!signals) {
      signals = {};
    }
    const signalKey = ctx.state.command.args[0];
    if (!signalKey) {
      replyMessage(ctx, 'Please enter a valid signal!');
      return;
    }
    const signalStr = signalToString(signalKey);
    if (!signalStr) {
      replyMessage(ctx, 'Please enter a valid signal!');
      return;
    }
    signals[signalKey] = signalStr;
    setStorage(`signalData.${userId}`, signals);
    replyMessage(ctx, 'Signal added successfully!');
  });

  bot.command('viewsignal', ctx => {
    const userId = ctx.update.message.from.id;
    const signals = getStorage(`signalData.${userId}`, {});
    let message = '';
    if (!!signals && Object.keys(signals).length) {
      message += 'Your signals:\n';
      Object.keys(signals).forEach(signalKey => {
        message += signalKey + '\n';
      });
    } else {
      message = `You haven't added any signal yet. Use /setsignal {signal} to add`;
    }
    replyMessage(ctx, message);
  });

  bot.command('csv', ctx => {
    const lastChartData: Array<ChartDataType> = getStorage('lastChartData', []);
    let headerString = 'Time, Open, Close, Result';
    let resultString = '';

    const userId = ctx.update.message.from.id;
    const signals = getStorage(`signalData.${userId}`, {});
    if (!!signals && Object.keys(signals).length) {
      Object.keys(signals).forEach(signalKey => {
        headerString += ', ' + signalKey;
      });
    }

    const rows = [];
    for (let i = 0; i < lastChartData.length; i++) {
      const data = lastChartData[i];
      if (!data[6]) {
        continue;
      }
      resultString += data[5] === 1 ? '1' : '0';
      let rowStr = `_${data[0]}, ${data[2]}, ${data[3]}, ${
        data[5] === 1 ? 'B' : 'S'
      }`;
      if (!!signals && Object.keys(signals).length) {
        Object.keys(signals).forEach(signalKey => {
          const signalStr = signals[signalKey];
          const strToCompare = resultString.slice(-signalStr.length);
          if (strToCompare === signalStr) {
            rowStr += ', 111';
          } else {
            rowStr += ', 000';
          }
        });
      }
      rows.push(rowStr);
    }
    const header = [headerString];
    const fileData = header.concat(rows).join('\n');
    const buf = Buffer.from(fileData, 'utf8');
    ctx.replyWithDocument({
      source: buf,
      filename: 'result.csv',
    });
  });
};
