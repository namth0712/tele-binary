#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
PURPLE='\033[1;35m'
LCYAN='\033[1;36m'
NC='\033[0m' # No Color

RED='==='
GREEN='==='
YELLOW='==='
BLUE='==='
PURPLE='==='
LCYAN='==='
NC='===' # No Color

echo -e "${GREEN}Project pre commit hook start${NC}"
echo -e "${GREEN}==============================${NC}"

# Get php files from staged
FILES_TO_CHECK=""
FILES=$(git diff --cached --name-only --diff-filter=ACM | grep ".js\{0,1\}$")
if [ "$FILES" == "" ]; then
    echo -e "${GREEN}NO FILES TO CHECK${NC}"
    exit 0
fi
for FILE in $FILES; do
    if [ -f "$FILE" ]; then
        FILES_TO_CHECK="$FILES_TO_CHECK $FILE"
    fi
done

if [ "$FILES_TO_CHECK" == "" ]; then
    echo -e "${GREEN}NO FILES TO CHECK${NC}"
    exit 0
fi

echo -e "${BLUE}Running lint...${NC}"

LINT_CHECK="$(npx eslint "src/**/*.ts" --fix && echo $?)"
if [[ "$LINT_CHECK" == 0 ]]; then
    echo -e "${RED}ESLint Passed.${NC}"
else
    echo -e $LINT_CHECK
    echo -e "${RED}ESLint Failed.${NC}"
    exit 1
fi

echo -e "${BLUE}Running build...${NC}"

npm run build

echo -e "${BLUE}Re-add new file...${NC}"

git add $FILES_TO_CHECK

git add dist

echo -e "${GREEN}==============================${NC}"
echo -e "${GREEN}Project pre commit hook finish${NC}"
